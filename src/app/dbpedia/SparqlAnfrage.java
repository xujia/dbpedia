package app.dbpedia;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
//import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Environment;
import android.util.Log;
//Semantische Suche mit SPARQL und DBpedia
public class SparqlAnfrage{
	
	String result = null;
	String vector = null;
	String contains = null;
	String relation = "?s1textp";
	String query = null;
	static final String ACTIVITY_TAG="SparqlQuery";
	
	//einfache semantische Suche nach "Entity", "Relation", "Value" mit dem eingegebenen Suchtext
	public String searchTriple(String selectQuery){
		//"query" wird enkodiert, so dass Sonderzeichen von Browsern interpretiert werden k�nnen. 
		String URLQuery = null;
		try {
			URLQuery = java.net.URLEncoder.encode(selectQuery, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		URLQuery = "http://dbpedia.org/sparql?default-graph-uri=&query="+URLQuery+"&format=json&timeout=30000";
		result = searchQuery(URLQuery);
		//"such_ergebnis" wird an den Aufrufer zur�ckgegeben.	
	return result;
	}
	
	public int countTriple(String selectQuery){
		int n = 0;
		String URLQuery = null;
		try {
			URLQuery = java.net.URLEncoder.encode(selectQuery, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		URLQuery = "http://dbpedia.org/sparql?default-graph-uri=&query="+URLQuery+"&format=json&timeout=30000";
		result = searchQuery(URLQuery);
		n = count(result);
		return n;
	}
	
	public String searchDBData(String str, int offset){
		/*SELECT DISTINCT ?Property ?Value ?isPropertyOf ?isValue 
				WHERE {{ <http://......>  ?Property ?Value.}
				      union
				      {?isValue ?isPropertyOf  <http://......>.}}
		  order  by (<LONG::IRI_RANK>(?Property))
		  limit 20 offset 0
		*/
		query = "select distinct ?Property ?Value ?isPropertyOf ?isValue where { {<"+str+">  ?Property ?Value.} union {?isValue ?isPropertyOf <"+str+">.}} order  by (<LONG::IRI_RANK>(?Property)) limit 20 offset "+ offset;
		Log.v(SparqlAnfrage.ACTIVITY_TAG, query);
		String URLQuery = null;
		try {
			URLQuery = java.net.URLEncoder.encode(query, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		URLQuery = "http://dbpedia.org/sparql?default-graph-uri=&query="+URLQuery+"&format=json&timeout=30000";
		result = searchQuery(URLQuery);	
		return result;
	}
	
	public int countDBdata(String str){
		/*select count distinct * where 
			 {{ <http://......>  ?Property ?Value.}
			 union
			 {?isValue ?isPropertyOf  <http://......>.}}
	    */
		int n = 0;
		query = "select count  distinct *  where { {<"+str+">  ?Property ?Value.} union {?isValue ?isPropertyOf <"+str+">.}}";
		Log.v(SparqlAnfrage.ACTIVITY_TAG, query);
		String URLQuery = null;
		try {
			URLQuery = java.net.URLEncoder.encode(query, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		URLQuery = "http://dbpedia.org/sparql?default-graph-uri=&query="+URLQuery+"&format=json&timeout=30000";
		result = searchQuery(URLQuery);
		n = count(result);
		return n;
	}
	
	private int count(String str){	
		int sum = 0;
		try {   
    		JSONObject dbjsonOb = new JSONObject(result).getJSONObject("results");  
    		JSONArray dbjsonArray = dbjsonOb.getJSONArray("bindings");     
            
            	JSONObject count = dbjsonArray.getJSONObject(0);
            	JSONObject m = count.getJSONObject("callret-0");
            	sum = m.getInt("value");
                
        } catch (JSONException e) {   
            System.out.println("Jsons parse error !");   
            e.printStackTrace();   
        }   
	
	return sum;
	}

	private String searchQuery(String search_sparql){
		StringBuffer readOneLineBuff = new StringBuffer();
		String ergebnis = null;
		try {
		URL url = new URL(search_sparql);
		URLConnection conn = url.openConnection();
		BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));		
		String line = null;
		
		while ((line = reader.readLine()) != null) {
			readOneLineBuff.append(line);
		}
		ergebnis = readOneLineBuff.toString();
		reader.close();	
		} 
		catch (MalformedURLException e) {
		e.printStackTrace();
		ergebnis = "No Result! This query did not produce any results. Try dropping some of the conditions, to make the query less specific.";
		}
		catch (IOException e2) {
		e2.printStackTrace();
		ergebnis = "No Result! This query did not produce any results. Try dropping some of the conditions, to make the query less specific.";
		}
		return ergebnis;
	}
	
}