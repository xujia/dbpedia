package app.dbpedia;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioGroup;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class SemanticSearch extends Activity {
	private ListView searchlist2;
	private ListView conditionList;
	private TextView note2;
	private View footView2;
	private View popupwindowView;  
	private PopupWindow popupWindow; 
	private RadioGroup radioGroup;
	private Button option;
	private boolean optionView = false;
	private String noteString = null;
	private String data2 = null;
	private String keywords = null;
	private String relation = "any";
	private String relationNote = "";
	private String relationUrl = "?s1textp";
	private String typNote = "";
	private String typUrl = "";
	private String typQuery = "";
	private String vector = null;
	private String contains = null;
	private String filterLabelQuery = null;
	private String conQuery = null;
	private ArrayList<String> tripleArrayList;
	private ArrayList<String> tripleNote;
	private String tripleQuery = "";
	private String queryNote = "";
	private String tripleQuery1 = null;
	private String whereQuery = null;
	private String selectQuery = null;
	private String countQuery = null;
	private String conNote = null;
	private int f = 2;
	private int sum = 0;
	private int a = 0;
	private int b = 0;
	private Button back_button;
	private Button menu_button;
	private Spinner spinner;
	private ArrayAdapter<CharSequence> adapter_spinner;
	private SparqlAnfrage sparql = new SparqlAnfrage();
	
	static final String ACTIVITY_TAG="SparqlQuery";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.layout_semantic_search);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.title_window_search);
        initView2();
        
    }
 
    private void initView2(){
    	
    	option = (Button) findViewById(R.id.SuchOptionButton);
        searchlist2 = (ListView) findViewById(R.id.SearchListView2);
        conditionList = (ListView) findViewById(R.id.SearchListView2);
        note2 = (TextView) findViewById(R.id.TextView2);
        
        tripleArrayList = new ArrayList<String>();
    	tripleNote = new ArrayList<String>();
        footView2 = getLayoutInflater().inflate(R.layout.foot_listview, null);
        //"text" von Activity DBpedia zu emfangen
        keywords = this.getIntent().getStringExtra("text");
        //Leerzeichen f�r den Suchtext zu ersetzen
        vector = keywords.replaceAll("\\s","', '");
    	contains = keywords.replaceAll("\\s", "\" AND \"");
    	relationNote = "<br><b><font color='Blue'>Entity</font></b> has Relation:<b><font color='#008000'>"+relation+"</font></b> with Value: "+ "<b>"+ keywords +"</b>.";
    	tripleQuery1 = "?s1 "+ relationUrl +" ?o1 . ?o1 bif:contains '(\""+contains+"\")' option ( score ?sc ).";
    	tripleArrayList.add(tripleQuery1);
    	tripleNote.add(relationNote);
    	filterLabelQuery = "?s1 rdfs:label ?Label. filter(langMatches(lang(?Label),\"en\"))";
    	
        //Suchoptionen mit den Werten, die in "String.xml" gespeichert sind, werden als Spinner erstellt. 
        spinner = (Spinner)findViewById(R.id.SuchOptionSpinner);
        adapter_spinner = ArrayAdapter.createFromResource(
                this, R.array.SuchOption, android.R.layout.simple_spinner_item);
        adapter_spinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter_spinner);
        //Aktionen von den SpinnerItem definieren
        OnItemSelectedListener oisl=  new OnItemSelectedListener()   
        {    
             public void onItemSelected(AdapterView<?> parent, View view, int position, long id)   
             {    
            	 switch (position) {
         	    case 0:
         	    	matchEntities(0);
     	            break;
     	        case 1:
     	        	distinctEntities(0);
     	            break;
            	 }
             }    
             public void onNothingSelected(AdapterView<?> parent) {}    
         };    
         spinner.setOnItemSelectedListener(oisl);
         
         //Beim Klicken �ffnet sich das Fenster "popupwindow"
         option.setOnClickListener(new OnClickListener(){
             @SuppressWarnings("deprecation")
			public void onClick(View v){
            	 popupwindowView = getLayoutInflater().inflate(R.layout.popup_window, null); 
            	 popupWindow = new PopupWindow(popupwindowView, 370, 400); //Fenstergr��e zu definieren
            	 popupWindow.setFocusable(true);  
            	 popupWindow.setOutsideTouchable(true); //"PopupWindow" zu schlie�en beim Ber�hren au�erhalb des Fensters
            	 popupWindow.setBackgroundDrawable(new BitmapDrawable());  
            	 popupWindow.showAtLocation((View)v.getParent(), Gravity.CENTER, Gravity.CENTER, Gravity.CENTER);//Fensterposition zu definieren
            	 
            	 radioGroup = (RadioGroup) popupwindowView.findViewById(R.id.radioGroup);  
            	 radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {  
            		 public void onCheckedChanged(RadioGroup group, int checkedId) {
            			 if (checkedId==R.id.sRel){
            				 setRelation(0);
            			 }
            			 if (checkedId==R.id.addET){
            				 setEntityTypes(0);
            			 }
            			 if (checkedId==R.id.addRS){
            				 addRelationSub(0);
            			 }
            			 if (checkedId==R.id.addRO){
            				 addRelationOb(0);
            			 }
            			 popupWindow.dismiss();
            	 }
                 });
             }
          });
         
        //TitelBar mit zwei ImageButton(back_button und menu_button)
        back_button = (Button) findViewById(R.id.back2);
        back_button.setOnClickListener(new OnClickListener(){
            public void onClick(View v){
            	SemanticSearch.this.finish();//Das Programm geht zu der letzten Activity zur�ck.
            }
        });
        menu_button = (Button) findViewById(R.id.setting2);
        menu_button.setOnClickListener(new OnClickListener(){
            public void onClick(View v){
            	openOptionsMenu();
            }
        });
    }
    
    //Suchen nach "Entities", die mit irgendeiner Relation "text" enthalten.
    private void matchEntities(int offset){
    	
    	String tripel = "";
    	for (int i = 0; i < tripleArrayList.size(); i++) {
    		tripel += tripleArrayList.get(i);
    	}
    	tripleQuery = tripel;
    	
    	String note = "";
    	for (int i = 0; i < tripleNote.size(); i++) {
    		note += tripleNote.get(i);
    	}
    	queryNote = note;
    	
    	if (relation.equals("any"))
    	viewEntityRelValue(offset);
    	else viewEntityValue(0);
    	}
    
    
    //"Entity", "Relation" und "Data" werden als ListView auf dem Bildschirm gezeigt.
    @SuppressWarnings("deprecation")
	private void viewEntityRelValue(int offset){
    	whereQuery = "select ?s1, ?Label, ?s1textp, ( ?sc * 3e-1 ) as ?sc, ?o1 where {" + tripleQuery + filterLabelQuery + "}";
		selectQuery = "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> select ?Label, ?s1 as ?Entity, ?s1textp as ?Relation, ( bif:search_excerpt ( bif:vector ( '"+vector+"' ) , ?o1 ) ) as ?Data where {" + whereQuery + " order by desc ( ?sc * 3e-1 + sql:rnk_scale ( <LONG::IRI_RANK> ( ?s1 ) ) ) } limit 20 offset ";
		
    	//Aufruf der Methode "EntityRelationValue() " von Klasse SparqlAnfrage f�r semantische Suche
    	//Das zur�ckgegebene Wert von Suchergebnis wird in die Variable "data2" gespeichert.
    	
    	countQuery = "select count distinct * where{" + tripleQuery + "}";
    	Log.v(SemanticSearch.ACTIVITY_TAG, countQuery);
    	sum = sparql.countTriple(countQuery);
    	
		a = offset+1;
    	b = offset+20;
		if (sum==0) a = 0;
    	if (sum<=b) b = sum;
    	note2.setText(Html.fromHtml("Ranked Entity Names and Text Summaries where:<i>"+queryNote+"</i><br>show "+a+" - "+b+" of total "+ sum));
    	selectQuery = selectQuery+offset;
    	data2 = sparql.searchTriple(selectQuery);
    	Log.v(SemanticSearch.ACTIVITY_TAG, selectQuery);
    	footView(0, offset);
    	
    	ArrayList<HashMap<String, Object >> list = new ArrayList<HashMap<String, Object >>(); 
    	HashMap<String, Object > map = null ;
    	try {   
    		JSONObject jsonObjR = new JSONObject(data2).getJSONObject("results");  
    		JSONArray jsonArrayB = jsonObjR.getJSONArray("bindings");     
            for(int i = 0; i < jsonArrayB.length() ; i++){   
            	JSONObject item = jsonArrayB.getJSONObject(i);
            	JSONObject entity = item.getJSONObject("Entity");
            	JSONObject label = item.getJSONObject("Label");
            	JSONObject relation = item.getJSONObject("Relation");
            	JSONObject data = item.getJSONObject("Data");
            	String Label = label.getString("value");
            	String Entity = entity.getString("value");
            	String Relation = relation.getString("value");
            	String Data = data.getString("value");
            	int index = Relation.lastIndexOf("/"); 
            	Relation = Relation.substring(index + 1); 
            	
                map = new HashMap<String, Object>();  
                map.put("Label", Label);  
                map.put("decodeEntityUrl", URLDecoder.decode(Entity));  
                map.put("EntityUrl", Entity);  
                map.put("ItemRelation", URLDecoder.decode(Relation));  
                map.put("ItemData", Html.fromHtml(URLDecoder.decode(Data)));
                list.add(map);  
            }                    
        } 
    	catch (JSONException e) {   
            System.out.println("Jsons parse error !");   
            e.printStackTrace();   
        }   
    	final SimpleAdapter mSchedule = new SimpleAdapter (SemanticSearch.this, list,   
                R.layout.ent_rel_val_listview,  
                new String[] {"Label", "decodeEntityUrl", "ItemRelation", "ItemData"},   
                new int[] {R.id.EntityLabel, R.id.decodeEntityUrl, R.id.ItemRelation, R.id.ItemData});  
    	    	
    	searchlist2.setAdapter(mSchedule);
		searchlist2.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {  
			ListView lv = (ListView)parent;  
			@SuppressWarnings("unchecked")
			HashMap<String,Object> dbr =(HashMap<String,Object>)lv.getItemAtPosition(position);   
			String dbEntity = (String) dbr.get("EntityUrl");
			
			Intent intent = new Intent();
            intent.setClass(SemanticSearch.this, SemanticResource.class);
            intent.putExtra("ItemEntity",dbEntity);
            startActivity(intent);
			}
		});
    }
    
    //Suchen nach "Entities", die mit irgendeiner Relation "text" enthalten, ohne Redunanzen zu liefern.
    private void distinctEntities(int offset){
    	selectQuery = "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> select ?Label ?s1 as ?Entity count ( * ) as ?count where {" + tripleQuery + filterLabelQuery + "} group by ?s1 ?Label order by desc 3 limit 20 offset ";
    	Log.v(SemanticSearch.ACTIVITY_TAG, selectQuery);
    	
    	countQuery = "select count (distinct ?s1) where {"+ tripleQuery + "}";
    	Log.v(SemanticSearch.ACTIVITY_TAG, selectQuery);
    	sum = sparql.countTriple(countQuery);
    	disEntityValueView(0);
    	}
    
    @SuppressWarnings("deprecation")
	private void disEntityValueView(int offset){
    	a = offset+1;
    	b = offset+20;
    	if (sum==0) a = 0;
    	if (sum<=b) b = sum;
    	data2 = sparql.searchTriple(selectQuery+offset);
    	note2.setText(Html.fromHtml("Distinct Entity Names ordered by Count where:<i>"+queryNote+"</i><br>show "+a+" - "+b+" of total "+ sum));
    	footView(1, offset);
    	ArrayList<HashMap<String, Object >> list_subject = new ArrayList<HashMap<String, Object >>(); 
    	HashMap<String, Object > map3 = null ;
    	try {   
    		JSONObject dbjsonOb = new JSONObject(data2).getJSONObject("results");  
    		JSONArray dbjsonArray = dbjsonOb.getJSONArray("bindings");     
            for(int i = 0; i < dbjsonArray.length() ; i++){   
            	JSONObject item3 = dbjsonArray.getJSONObject(i);
            	JSONObject DisEntity = item3.getJSONObject("Entity");
            	JSONObject label = item3.getJSONObject("Label");
            	JSONObject Count = item3.getJSONObject("count");
            	String disEntity = DisEntity.getString("value");
            	String Label = label.getString("value");
            	int count = Count.getInt("value");
            	                        
                map3 = new HashMap<String, Object>();  
                map3.put("Label", Label);  
                map3.put("decodeEntityUrl", URLDecoder.decode(disEntity));  
                map3.put("EntityUrl", disEntity);
                map3.put("Count", "Occurrence Count: "+count);  
                list_subject.add(map3);  
            }                    
        } catch (JSONException e) {   
            System.out.println("Jsons parse error !");   
            e.printStackTrace();   
        }   
    	final SimpleAdapter mSchedule3 = new SimpleAdapter (this, list_subject,   
                R.layout.ent_val_listview,  
                new String[] {"Label", "decodeEntityUrl", "Count"},   
                new int[] {R.id.extractEntityLabel, R.id.extractEntityUrldecode, R.id.extractValue});  
    	
		searchlist2.setAdapter(mSchedule3);    
        searchlist2.setOnItemClickListener(new OnItemClickListener() {

    	  @SuppressWarnings("unchecked")
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {  
    		ListView lv = (ListView)parent;  
			HashMap<String,Object> dbr =(HashMap<String,Object>)lv.getItemAtPosition(position);   
			String disEntityUrl = (String) dbr.get("EntityUrl");
			
			Intent intent = new Intent();
            intent.setClass(SemanticSearch.this, SemanticResource.class);
            intent.putExtra("ItemEntity",disEntityUrl);
            startActivity(intent);
		}
      });
  	}
    
    //Suchen nach Typen von "Entities"
    private void setEntityTypes(int offset){
    	
    	int a = offset+1;
    	int b = offset+20;
    	if (sum==0) a = 0;
    	if (sum<=b) b = sum;
    	String typEntity = tripleQuery + "?s1 a ?s1c .";
    	selectQuery = "select ?s1c as ?c1 count ( * ) as ?c2 where {" + typEntity + "} group by ?s1c order by desc 2 limit 20 offset " + offset;
    	Log.v(SemanticSearch.ACTIVITY_TAG, selectQuery);
    	data2 = sparql.searchTriple(selectQuery);
    	countQuery = "select count (distinct ?s1c) where {" + typEntity + "}";
    	Log.v(SemanticSearch.ACTIVITY_TAG, selectQuery);
    	sum = sparql.countTriple(countQuery);
    	note2.setText(Html.fromHtml("Select Types of Entity where:<i>"+queryNote+"</i><br>show "+a+" - "+b+" of total "+ sum));
    	setOption(offset, 2);
    }
    
    //Suchen nach Relationen, die Beziehungen zwischen "Entities" sind.
    private void setRelation(int offset){
    	int a = offset+1;
    	int b = offset+20;
    	if (sum==0) a = 0;
    	if (sum<=b) b = sum;
    	relationUrl = "?s1textp";
    	tripleQuery1 = "?s1 "+ relationUrl +" ?o1 . ?o1 bif:contains '(\""+contains+"\")' option ( score ?sc ).";
    	tripleArrayList.set(0,tripleQuery1);
    	String tripel = "";
    	for (int i = 0; i < tripleArrayList.size(); i++) {
    		tripel += tripleArrayList.get(i);
    	}
    	tripleQuery = tripel;
    	selectQuery = "select ?s1textp as ?c1 count ( * ) as ?c2 where {" + tripleQuery + "} group by ?s1textp order by desc 2 limit 20 offset "+ offset;
    	data2 = sparql.searchTriple(selectQuery);
    	countQuery = "select count (distinct ?s1textp) where { "+tripleQuery + "}";
    	sum = sparql.countTriple(countQuery);
    	if (sum==0) a = 0;
    	if (sum<=b) b = sum;
    	relation = "any";
    	relationNote = "<br><b><font color='Blue'>Entity</font></b> has Relation:<b><font color='#008000'>"+relation+"</font></b> with Value: "+ "<b>"+ keywords +"</b>.";
    	tripleNote.set(0, relationNote);
    	String note1 = "";
    	for (int i = 1; i < tripleNote.size(); i++) {
    		note1 += tripleNote.get(i);
    	}
    	note2.setText(Html.fromHtml("Select a Relation for Entity  with Value: "+ "<b><br>"+ keywords +"</b>."+note1+"<br>show "+a+" - "+b+" of total "+ sum));
    	setOption(offset, 3);
    }
    
    private void addRelationSub(int offset){
    	int a = offset+1;
    	int b = offset+20;
    	if (sum==0) a = 0;
    	if (sum<=b) b = sum;
    	String subEntity = tripleQuery + "?s1 ?s1p ?s1s .";
    	selectQuery = "select ?s1p as ?c1 count ( * ) as ?c2 where {" + subEntity + "} group by ?s1p order by desc 2 limit 20 offset " + offset;
    	data2 = sparql.searchTriple(selectQuery);
    	countQuery = "select count(distinct ?s1p) where {" + subEntity + "}";
    	sum = sparql.countTriple(countQuery);
    	note2.setText(Html.fromHtml("Add a new Relation and Entity is subject of Relation.<i>"+queryNote+"</i><br>show "+a+" - "+b+" of total "+ sum));
    	setOption(offset, 4);
    }
    
    //
    private void addRelationOb(int offset){
    	int a = offset+1;
    	int b = offset+20;
    	if (sum==0) a = 0;
    	if (sum<=b) b = sum;
    	String obEntity = tripleQuery + "?s1o ?s1ip ?s1 .";
    	selectQuery = "select ?s1ip as ?c1 count ( * ) as ?c2 where {" + obEntity + "} group by ?s1ip order by desc 2 limit 20 offset " + offset;
    	data2 = sparql.searchTriple(selectQuery);
    	countQuery = "select count(distinct ?s1ip) where {" + obEntity + "}";
    	sum = sparql.countTriple(countQuery);
    	
    	note2.setText(Html.fromHtml("Add a new Relation and Entity is object of Relation.<i>"+queryNote+"</i><br>show "+a+" - "+b+" of total "+ sum));
    	setOption(offset, 5);
    }
    
    //Relationen oder Entity-Typen werden als ListView gezeigt.
    @SuppressWarnings("deprecation")
	private void setOption(int offset, final int m){
    	
    	footView(m, offset);//footView im ListView hinzuf�gen
    	ArrayList<HashMap<String, Object >> list_subject = new ArrayList<HashMap<String, Object >>(); 
    	HashMap<String, Object > map3 = null ;
    	try {   
    		JSONObject dbjsonOb = new JSONObject(data2).getJSONObject("results");  
    		JSONArray dbjsonArray = dbjsonOb.getJSONArray("bindings");     
            for(int i = 0; i < dbjsonArray.length() ; i++){   
            	JSONObject item3 = dbjsonArray.getJSONObject(i);
            	JSONObject c1 = item3.getJSONObject("c1");
            	JSONObject c2 = item3.getJSONObject("c2");
            	String C1 = c1.getString("value");
            	int C2 = c2.getInt("value");
                map3 = new HashMap<String, Object>();  
                map3.put("Option", URLDecoder.decode(C1));  
                map3.put("Value", C1);  
                map3.put("Count", "Count: "+C2);  
                list_subject.add(map3);  
            }                    
        } catch (JSONException e) {   
            System.out.println("Jsons parse error !");   
            e.printStackTrace();   
        }   
    	final SimpleAdapter mSchedule3 = new SimpleAdapter (this, list_subject,   
                R.layout.option_count_listview,  
                new String[] {"Option", "Count"},   
                new int[] {R.id.twoItem1,R.id.twoItem2});  
    	
    	optionView = true;
		searchlist2.setAdapter(mSchedule3);
		searchlist2.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) { 
			optionView = false;
			ListView lv = (ListView)parent;  
			@SuppressWarnings("unchecked")
			HashMap<String,Object> dbr =(HashMap<String,Object>)lv.getItemAtPosition(position);   
			
			//EntityTyp
			if (m==2){
				typUrl = (String) dbr.get("Value"); 
				String decodeTypUrl = (String) dbr.get("Option");
				int index_typ = decodeTypUrl.lastIndexOf("/"); 
				typNote = "<br><b><font color='Blue'>Entity</font></b> has Type:"+"<b>"+decodeTypUrl.substring(index_typ + 1)+"</b>";
				typQuery = "?s1 a <"+typUrl+">."; 
				tripleArrayList.add(typQuery);
		    	tripleNote.add(typNote);
		    	int spId = (int) spinner.getSelectedItemId(); 
		    	if (spId==0){matchEntities(0);}
		    	else if (spId==1){spinner.setSelection(0);}
		    	
			}
			//Relation
			if (m==3){
				relationUrl = (String) dbr.get("Value");
				String decodeRelUrl = (String) dbr.get("Option");
				int index_rel = decodeRelUrl.lastIndexOf("/"); 
				relation = decodeRelUrl.substring(index_rel + 1);	
				relationNote = "<br><b><font color='Blue'>Entity</font></b> has Relation:<b><font color='#008000'>"+relation+"</font></b> with Value: "+ "<b>"+ keywords +"</b>.";

				relationUrl = "<"+relationUrl+">";
				tripleQuery1 = "?s1 "+ relationUrl +" ?o1 . ?o1 bif:contains '(\""+contains+"\")' option ( score ?sc ).";
		    	tripleArrayList.set(0,tripleQuery1);
		    	tripleNote.set(0,relationNote);
		    	int spId = (int) spinner.getSelectedItemId(); 
		    	if (spId==0){matchEntities(0);}
		    	else if (spId==1){spinner.setSelection(0);}
			}
			//Subject
			if (m==4){
				String subRelUrl = (String) dbr.get("Value"); 
				String decodeSubRelUrl = (String) dbr.get("Option");
				int index_sub = decodeSubRelUrl.lastIndexOf("/"); 
				String subRelStr = decodeSubRelUrl.substring(index_sub + 1);
				conNote = "<br><b><font color='Blue'>Entity</font></b> has Relation:<b><font color='#008000'>" + subRelStr + "</font></b> with Value:";
				
				String subRelationQuery = "?s1 <" + subRelUrl + "> ?s"+f+"."; 
				conQuery = subRelationQuery;
				selectQuery = "select distinct ?s"+f+" as ?c1 where  {"+ tripleQuery + subRelationQuery + "} order by desc ( <LONG::IRI_RANK> ( ?s"+f+" ) ) limit 20 offset ";
				countQuery = "select count distinct (?s"+f+")  where {" + tripleQuery + subRelationQuery + "}" ;
				sum = sparql.countTriple(countQuery);
				noteString = "Select a value or condition where: <br><b><font color='Blue'>Entity</font></b> has Relation: <b><font color='#008000'>"+subRelStr+"</font></b> with Value.<i>"+queryNote+"</i>";
		    	addCondition(0);
			}
			//Objekt
			if (m==5){
				String obRelUrl = (String) dbr.get("Value"); 
				String decodeObRelUrl = (String) dbr.get("Option");
				int index_ob = decodeObRelUrl.lastIndexOf("/"); 
				String obRelStr = decodeObRelUrl.substring(index_ob + 1);
				conNote = "<br><b><font color='Blue'>Entity</font></b> is Relation:<b><font color='#008000'>" + obRelStr + "</font></b> of Value:";
				
				String obRelationQuery = "?s"+f+" <" + obRelUrl + "> ?s1."; 
				conQuery = obRelationQuery;
			 	
				selectQuery = "select distinct ?s"+f+" as ?c1 where  {"+ tripleQuery + obRelationQuery + "} order by desc ( <LONG::IRI_RANK> ( ?s"+f+" ) ) limit 20 offset ";
				countQuery = "select count distinct (?s"+f+")  where {" + tripleQuery + obRelationQuery + "}" ;
				sum = sparql.countTriple(countQuery);
				noteString = "Select a value or condition, which has Relation: <br><b><font color='#008000'>"+obRelStr+"</font></b> with <b><font color='Blue'>Entity</font></b>.<i>"+queryNote+"</i>";
				
		    	addCondition(0);
			}
			}
		});
    }
 private void addCondition(int offset){
	 a = offset+1;
 	 b = offset+20;
 	if (sum==0) a = 0;
	if (sum<=b) b = sum;
 	 data2 = sparql.searchTriple(selectQuery+offset);
 	 String conString = noteString + "<br>show "+a+" - "+b+" of total "+ sum;
 	 note2.setText(Html.fromHtml(conString));

	 footView(7, offset);
	 ArrayList<HashMap<String, Object >> add_liste = new ArrayList<HashMap<String, Object >>();
	 
	 String condition = null;
	 HashMap<String, Object > map3 =  null;
 	 try {   
 		JSONObject jsonObjR = new JSONObject(data2).getJSONObject("results");  
 		JSONArray jsonArrayB = jsonObjR.getJSONArray("bindings");     
         for(int i = 0; i < jsonArrayB.length() ; i++){   
         	JSONObject item = jsonArrayB.getJSONObject(i);
         	JSONObject c1 = item.getJSONObject("c1");
         	 
         	String conType = c1.getString("type");
         	if (conType.equals("uri")){
         		@SuppressWarnings("deprecation")
				String deConValue = URLDecoder.decode(c1.getString("value"));
         		String conValue = c1.getString("value");
         		condition = "<"+conValue+">";
         		map3 =  new HashMap<String, Object>();
         		String regex = "^http://dbpedia.org/resource/.*";
              	boolean isDBurl = Pattern.matches(regex, conValue);
              	if (isDBurl){
              		map3.put("deValue", "... "+deConValue);
              	}
              	else map3.put("deValue", deConValue);
              	map3.put("Value", conValue);
         		map3.put("Type", conType); 
         		map3.put("Condition", condition); 
         	}
         	else if (conType.equals("literal")){
         		String conLang = c1.getString("xml:lang");
         		String conValue = c1.getString("value");
         		@SuppressWarnings("deprecation")
				String deConValue = URLDecoder.decode(c1.getString("value"));
         		condition = "\"\"\""+conValue+"\"\"\"@"+conLang;
         		map3 =  new HashMap<String, Object>();
         		map3.put("Type", conType); 
         		map3.put("Value", conValue+"@"+conLang); 
         		map3.put("deValue", deConValue);
         		map3.put("Condition", condition);
         	}
         	else if (conType.equals("typed-literal")){
         		String conValue = c1.getString("value");
         		@SuppressWarnings("deprecation")
				String deConValue = URLDecoder.decode(c1.getString("value"));
         		condition = conValue;
         		map3 =  new HashMap<String, Object>();
         		map3.put("Type", conType);
         		map3.put("Value", conValue); 
         		map3.put("deValue", deConValue);
         		map3.put("Condition", condition);
         	}
         	add_liste.add(map3);
         }                    
     } 
 	catch (JSONException e) {   
         System.out.println("Jsons parse error !");   
         e.printStackTrace();   
     }   
 	
 	final SimpleAdapter mSchedule3 = new SimpleAdapter (this, add_liste,   
            R.layout.one_listview,  
            new String[] {"deValue"},   
            new int[] {R.id.OneItem});
 	
 	conditionList.setAdapter(mSchedule3);
 	conditionList.setOnItemClickListener(new OnItemClickListener() {
			@SuppressWarnings("deprecation")
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {  
			ListView lv = (ListView)parent;  
			@SuppressWarnings("unchecked")
			HashMap<String,Object> dbr =(HashMap<String,Object>)lv.getItemAtPosition(position);   
			String condition = (String) dbr.get("Condition");
			String value = (String) dbr.get("Value");
			String conditionQuery = conQuery + " filter ( ?s"+f+" =" + condition + "). ";
			
			tripleArrayList.add(conditionQuery);
	    	f++;
	    	tripleNote.add(conNote+URLDecoder.decode(value));
	    	int spId = (int) spinner.getSelectedItemId(); 
	    	if (spId==0){matchEntities(0);}
	    	else if (spId==1){spinner.setSelection(0);}
	    	}
		});
 	conditionList.setOnCreateContextMenuListener(this);	
 }
//Mit gegebener Relation und Entity-Typen werden "Entity" und "Data" als ListView auf dem Bildschirm gezeigt.
@SuppressWarnings("deprecation")
private void viewEntityValue(int offset){
	//spinner.setSelection(0);
	whereQuery = "select ?Label, ?s1, ( ?sc * 3e-1 ) as ?sc, ?o1 where {" + tripleQuery + filterLabelQuery + "}";
	selectQuery = "select distinct ?Label, ?s1 as ?Entity, ( bif:search_excerpt ( bif:vector ( '"+vector+"' ) , ?o1 ) ) as ?Data where {" + whereQuery + " order by desc ( ?sc * 3e-1 + sql:rnk_scale ( <LONG::IRI_RANK> ( ?s1 ) ) )} limit 20 offset "+offset;
	 Log.v(SemanticSearch.ACTIVITY_TAG, selectQuery);
	data2 = sparql.searchTriple(selectQuery);
	 
	countQuery = "select distinct count(*) where {"+ tripleQuery + filterLabelQuery + "}";
	 Log.v(SemanticSearch.ACTIVITY_TAG, countQuery);
	sum = sparql.countTriple(countQuery);
	a = offset+1;
	b = offset+20;
	if (sum==0) a = 0;
	if (sum<=b) b = sum;
	note2.setText(Html.fromHtml("Ranked Entity Names and Text Summaries where:<i>"+queryNote+"</i><br>show "+a+" - "+b+" of total "+ sum));
	
	footView(6, offset);
	ArrayList<HashMap<String, Object >> list = new ArrayList<HashMap<String, Object >>(); 
	HashMap<String, Object > map = null ;
	try {   
		JSONObject jsonObjR = new JSONObject(data2).getJSONObject("results");  
		JSONArray jsonArrayB = jsonObjR.getJSONArray("bindings");     
        for(int i = 0; i < jsonArrayB.length() ; i++){   
        	JSONObject item = jsonArrayB.getJSONObject(i);
        	JSONObject label = item.getJSONObject("Label");
        	JSONObject entity = item.getJSONObject("Entity");
        	JSONObject data = item.getJSONObject("Data");
        	String Label = label.getString("value");
        	String Entity = entity.getString("value");
        	String Data = data.getString("value");
        	                        
            map = new HashMap<String, Object>();  
            map.put("Label", Label);  
            map.put("decodeEntityUrl", URLDecoder.decode(Entity));  
            map.put("EntityUrl", Entity);  
            map.put("Value", Html.fromHtml(URLDecoder.decode(Data)));
            list.add(map);  
        }                    
    } 
	catch (JSONException e) {   
        System.out.println("Jsons parse error !");   
        e.printStackTrace();   
    }   
	final SimpleAdapter mSchedule = new SimpleAdapter (SemanticSearch.this, list,   
            R.layout.ent_val_listview,  
            new String[] {"Label", "decodeEntityUrl", "Value"},   
            new int[] {R.id.extractEntityLabel, R.id.extractEntityUrldecode, R.id.extractValue});  
			searchlist2.setAdapter(mSchedule);
			searchlist2.setOnItemClickListener(new OnItemClickListener() {
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {  
				ListView lv = (ListView)parent;  
				@SuppressWarnings("unchecked")
				HashMap<String,Object> dbr =(HashMap<String,Object>)lv.getItemAtPosition(position);   
				String db = (String) dbr.get("EntityUrl");
				
				Intent intent = new Intent();
                intent.setClass(SemanticSearch.this, SemanticResource.class);
                intent.putExtra("ItemEntity",db);
                startActivity(intent);
				}
			});
			
    }
   
   
  
//Men�-Symbols f�r "History", "Favorites", "Add", "About"
  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
      getMenuInflater().inflate(R.menu.menu1, menu);
      return true;
  }
  @Override
  public void openOptionsMenu() {
          // TODO Auto-generated method stub
          super.openOptionsMenu();
  }
  
  public boolean onOptionsItemSelected(MenuItem item) {
  	        switch (item.getItemId()) {
  	        case R.id.item01:
  	        	revertCondition();
	            break;
  	        case R.id.item02:
  	        	resetCondition();
	            break;
	        case R.id.item03:
	        	help();
	            break;
  	        }
				return false;      
}

  //Die letzte Suchoption wird gel�scht.
private void revertCondition() {
	if (optionView){
		optionView = false;
	}
	else {
		if(tripleArrayList.size()>1) {
			tripleArrayList.remove(tripleArrayList.size()-1);
		}
		if (tripleNote.size()>1) {
		tripleNote.remove(tripleNote.size()-1);
		}
	}
	int spId = (int) spinner.getSelectedItemId(); 
	if (spId==0){matchEntities(0);}
	else if (spId==1){spinner.setSelection(0);}
	
}

//Alle Suchoptionen werden gel�scht.
private void resetCondition() {
	relationUrl = "?s1textp";
	tripleQuery1 = "?s1 "+ relationUrl +" ?o1 . ?o1 bif:contains '(\""+contains+"\")' option ( score ?sc ).";
	relation = "any";
	relationNote = "<br><b><font color='Blue'>Entity</font></b> has Relation:<b><font color='#008000'>"+relation+"</font></b> with Value: "+ "<b>"+ keywords +"</b>.";
	
	tripleArrayList.clear();
	tripleNote.clear();
	tripleArrayList.add(tripleQuery1);
	tripleNote.add(relationNote);
	int spId = (int) spinner.getSelectedItemId(); 
	if (spId==0){matchEntities(0);}
	else if (spId==1){spinner.setSelection(0);}
}

private void help() {
	final AlertDialog.Builder builder = new Builder(SemanticSearch.this);
	builder.setTitle("Help?");
	builder.setMessage(Html.fromHtml("You can filter the results (entities) using:<br><font color='#008000'>Select Relation:</font>select a relation for keywords and entities.<br><font color='#008000'>Add Entity Types:</font> add types for entities <br><font color='#008000'>Add Relation as Subject:</font> add a new relation for entities. Entities are subject of the relationship.<br><font color='#008000'>Add Relation as Object:</font> add a new relation for entities. Entities are object of the relationship."));
	builder.setNegativeButton("Close", new DialogInterface.OnClickListener() {
		   public void onClick(DialogInterface dialog, int which) {
			   }
		});
	AlertDialog alertDialog = builder.create();
	alertDialog.show();	
}

   //Durch langes Anklicken des ListView-Item wird ContextMenu erstellt.
@Override
  public void onCreateContextMenu(ContextMenu menu, View view, ContextMenuInfo menuInfo) {
      AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
	  @SuppressWarnings("unchecked")
	  HashMap<String,Object> dbr =(HashMap<String,Object>)conditionList.getItemAtPosition(info.position);
	  String conType = (String) dbr.get("Type");
	  String url = (String) dbr.get("deValue");
	  if (conType!=null){
		  if (conType.equals("uri")){
		    String regex = "^... http://dbpedia.org/resource/.*";
          	boolean isDBurl = Pattern.matches(regex, url);
          	if (isDBurl){
          		 menu.setHeaderTitle("Option");
          		 menu.add(1, 1, 0, "Open Link in DBpedia");
          		 menu.add(1, 2, 0, "Cancel");
          	}   
          	else return;
		  } 
	  }
     
  }
  
  //"Open Link in DBpedia" geklickt
  //Entity wird in DBpedia Ressource gesucht.
  @Override
  public boolean onContextItemSelected(MenuItem item){
	  AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();  
      int position = info.position;  
      @SuppressWarnings("unchecked")
	  HashMap<String,Object> dbr =(HashMap<String,Object>)conditionList.getItemAtPosition(position);   
      String value = (String) dbr.get("deValue");
      value = value.substring(4);
      
      if(item.getItemId()==1){
    	  Intent intent = new Intent();
          intent.setClass(SemanticSearch.this, SemanticResource.class);
          intent.putExtra("ItemEntity",value);
          startActivity(intent);
      }
      else {return false;} 
      return true;
  }
  
  //Back-Button und Next-Button im FootView zu definieren
	private void footView(int position, int nb){
		final int num = nb;
		final int p = position;
    	int a = num+1;
    	int b = num+20;
    	if (sum==0) a = 0;
    	if (sum<=b) b= sum;
    	
		 if(searchlist2.getFooterViewsCount()>0)
	  		{searchlist2.removeFooterView(footView2);}
	      	
  		searchlist2.addFooterView(footView2);
  		TextView text_unten = (TextView) findViewById(R.id.TextUnten);
  		text_unten.setText("show "+a+" - "+b+" of total "+ sum);
  		Button btn_next = (Button) findViewById(R.id.btn_Next);
  		if (sum>b){
  		btn_next.setOnClickListener(new OnClickListener(){
  			public void onClick(View v) {
  				if(p==0){viewEntityRelValue(num+20);}
  				if(p==1){disEntityValueView(num+20);}
  				if(p==2){setEntityTypes(num+20);}
  				if(p==3){setRelation(num+20);}
  				if(p==4){addRelationSub(num+20);}
  				if(p==5){addRelationOb(num+20);}
  				if(p==6){viewEntityValue(num+20);}
  				if(p==7){addCondition(num+20);}
  			}
  			});
  		}
	  		
  		Button btn_back = (Button) findViewById(R.id.btn_Previous);
  		if (num>=20){
  		btn_back.setOnClickListener(new OnClickListener(){
  			public void onClick(View v) {
  				if(p==0){viewEntityRelValue(num-20);}
  				if(p==1){disEntityValueView(num-20);}
  				if(p==2){setEntityTypes(num-20);}
  				if(p==3){setRelation(num-20);}
  				if(p==4){addRelationSub(num-20);}
  				if(p==5){addRelationOb(num-20);}
  				if(p==6){viewEntityValue(num-20);}
  				if(p==7){addCondition(num-20);}
  			}
  		});
  		}
	}
}