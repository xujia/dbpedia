package app.dbpedia;

import java.io.File;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;


public class DBpedia extends Activity {
	private String search_text = null;
	private AutoCompleteTextView autoComplete;
	private ListView hisList;
	private ListView favList;
	private TextView textview1;
	private Button search_btn;
	private Button menu_button;
	private String longhistory = null;
	private StringBuilder sb = null;
	private SharedPreferences sp;
	final SparqlAnfrage sparql = new SparqlAnfrage();
	
	//OnCreate() wird beim Start einer Activity aufgerufen.
    @Override//Die onCreate()-Methode wird überschrieben.
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);//Das aktuelle Fenster der Activity wird ersetzt.
        setContentView(R.layout.layout_dbpedia_main);//UI wird durch die Ressource eines Layouts "layout_dbpedia_main.xml" erstellt. 
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.title_window_home);//Layout für die Titelbar zu setzen 
        
        //um den Fehler "android.os.NetworkOnMainThreadException" zu vermeiden, der über Android 3.0 auftreten wird.
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
          }
        initView();
    }
     
    public void initView(){
        autoComplete= (AutoCompleteTextView) findViewById(R.id.SearchText1); 
        hisList = (ListView) findViewById(R.id.HisListView);
        favList = (ListView) findViewById(R.id.FavListView);
        textview1 = (TextView) findViewById(R.id.TextView1);
        search_btn = (Button) findViewById(R.id.SearchBtn1);
        
        //Ab dem ersten Zeichen werden die Vorschläge, die im Suchverlauf vorher gespeichert sind, angezeigt. 
        initAutoComplete("history", autoComplete);
        
        //Menü-Button in den Titelbar einzusetzen
        menu_button = (Button) findViewById(R.id.home_setting);
        menu_button.setOnClickListener(new OnClickListener(){
            public void onClick(View v){
            	openOptionsMenu();//Menü wird angezeigt.
            	}
            });
        
        search_btn.setOnClickListener(new OnClickListener(){
            public void onClick(View v){
            	//Suchtext wird beim Klicken des SearchButtons im Suchverlauf unter einem angegebenen Namen "history" abgelegt.
            	search_text = saveHistory("history", autoComplete);
            	
            	//Suchtext wird als "text" per Intent(Systemnachrichten) von Activity DBpedia nach Activity ErstSuche übergegeben. 
            	Intent intent = new Intent();
                intent.setClass(DBpedia.this, SemanticSearch.class);
                intent.putExtra("text",search_text);
                startActivity(intent);
            	}
        });
    }
    
   //Suchtext wird im Speicherbereich "Suchverlauf.xml" gelegt.
    private String saveHistory(String field, AutoCompleteTextView auto) {
    	// SuchString vom Eingaben zu erhalten, ohne Whitespace-Zeichen am Anfang oder am Ende
    	String text = auto.getText().toString().trim();  
    	
    	//Zugriff von Präferenzdaten unter "\data\PackageName\shared_prefs\Suchverlauf.xml" Das zweite Argument "Context.MODE_PRIVATE" verbietet anderen Applikationen den Zugriff auf diese Daten.
        sp = getSharedPreferences("Suchverlauf", Context.MODE_PRIVATE);  
        //String-Daten unter einem Namen "history" werden gelesen. Das zweite Argument ist ein Vorgabewert.
        longhistory = sp.getString(field, ",");  
        //Wenn Suchtext im "history" nicht vorhanden ist, dann wird es gespeichert.
        if (!longhistory.contains(","+text + ",")) {  
            sb = new StringBuilder(longhistory);  
            sb.insert(0, ","+text);  
            sp.edit().putString("history", sb.toString()).commit();  
        }  
		return text;
	}
  
    //Vorschläge, die im Speicherbereich "Suchverlauf.xml" gespeichert sind, werden auf der DropdownListe angezeigt. 
    private void initAutoComplete(String field,
			AutoCompleteTextView auto) {
	  sp = getSharedPreferences("Suchverlauf", Context.MODE_PRIVATE);  
      longhistory = sp.getString(field, "");  
      
      //Suchverlauf wird gegeliedert und in "histories" gespeichert.
      String[] histories = longhistory.split(","); 
      //Listeneinträge "histories" sollen zuerst in Adapter hinzugefügt werden.
      ArrayAdapter<String> adapter_auto = new ArrayAdapter<String>(this,  
    		  android.R.layout.simple_list_item_1, histories);  
      
      //Es werden nur die letzten 50 Suchtext gespeichert.
      if (histories.length > 50) {  
          String[] newHistories = new String[50];  
          System.arraycopy(histories, 0, newHistories, 0, 50);  
          adapter_auto = new ArrayAdapter<String>(this,  
                  android.R.layout.simple_list_item_1, newHistories);  
      }
      //ListView wird von dem Adapter aufgebaut und auf dem Bildschirm angezeigt.
      auto.setAdapter(adapter_auto);  
      //DropdownListe von Vorschläge klicken
      auto.setOnFocusChangeListener(new OnFocusChangeListener() {  
              public void onFocusChange(View v, boolean hasFocus) {  
                  AutoCompleteTextView view = (AutoCompleteTextView) v;  
                  if (hasFocus) {  
                      view.showDropDown();  
                  }  
              }  
      });  
    }  		
	
    //Menü zu erstellen
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	//die virtuelle Tastatur auszublenden
        InputMethodManager imm =(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
    	imm.hideSoftInputFromWindow(autoComplete.getWindowToken(), 0);
    	getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }
    @Override
    public void openOptionsMenu() {
    	InputMethodManager imm =(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
    	imm.hideSoftInputFromWindow(autoComplete.getWindowToken(), 0);
    	super.openOptionsMenu();
    }
    
    //Die Aktionen von Menü-Elementen werden definiert.
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
    	    case R.id.item01:
    	    	textview1.setTextSize(16);
	        	textview1.setGravity(Gravity.LEFT);
	        	textview1.setText("DBpedia is a crowd-sourced community effort to extract structured information from Wikipedia and make this information available on the Web. DBpedia allows you to ask sophisticated queries against Wikipedia, and to link the different data sets on the Web to Wikipedia data.\n\nDBpedia Mobile is a general purpose RDF query facility for facet based browsing for mobile devices. DBpdeia Mobile supports keyword-based semantic search. Frist you can enter a text pattern to look for. Semantic search engine will show a listing of entities with the text occurring. Then you can continue refine your search by select relation, add entity types, add relation as subject or add relation as object. The results are sorted using a combination of text match score and entity-attribute-value link coefficients.\n\nDBpedia Mobile use a public SPARQL endpoint over the DBpedia data set at http://dbpedia.org/sparql. The endpoint is provided using OpenLink Virtuoso as the back-end database engine.\n\nThere is a list of all DBpedia data sets that are currently loaded into the SPARQL endpoint.\nYou can ask queries against DBpedia using:\nthe Leipzig query builder at http://querybuilder.dbpedia.org;\nthe OpenLink Interactive SPARQL Query Builder (iSPARQL) at http://dbpedia.org/isparql;\nthe SNORQL query explorer at http://dbpedia.org/snorql (does not work with Internet Explorer); or any other SPARQL-aware client(s).");
	            break;
	        case R.id.item02:
	        	getHistory("history");
	            break;
	        case R.id.item03:
	        	getFavorites();
	        	break;
	        case R.id.item04:
	        	delHistoryAll();
	        	break;
	        case R.id.item05:
	        	delFavoritAll();
	        	break;
    	}
		return false;      
    }

    //Favoriten als ListView auf dem Bildschirm zu zeigen 
	private void getFavorites() {
		textview1.setTextSize(14);
		textview1.setGravity(Gravity.CENTER);
		textview1.setText("Favorites");
		
		if(null != hisList){
         hisList.setVisibility(View.GONE);
         favList.setVisibility(View.VISIBLE);
        }
		//Favoriten werden in der SD-Karte unter Ordner "DBpedia" gespeichert.
		if(!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
	        return; //Wenn SD-Karte nicht existiert, wird die Funktion von return beendet.
	    } 
		
		//Verzeichnispfad von Favoriten-Dateien definieren
		String path = Environment.getExternalStorageDirectory().toString();
		File pathFavorites = new File(path + File.separator +"DBpedia");
		
		File[] favoritesFiles = null; 
		ArrayList<HashMap<String, Object >> list1 = new ArrayList<HashMap<String, Object >>(); 
    	HashMap<String, Object > map1 = null ;
		
		//Namen von Favoriten-Dateien werden als ListView auf dem Bildschirm angezeigt.
		if (pathFavorites.exists()) { 
			favoritesFiles = pathFavorites.listFiles(); 
	          int count = favoritesFiles.length;// 
	                for (int i = 0; i < count; i++)   
	                {    
	                    File file = favoritesFiles[i];
	                    String favorName = file.getName();
	                    map1 = new HashMap<String, Object>();  
	                    map1.put("favorName", favorName);  
	                    map1.put("deFavorName", URLDecoder.decode(favorName)); 
	                    list1.add(map1);
	                } 
	         
            final SimpleAdapter mSchedule1 = new SimpleAdapter (this, list1,   
                    R.layout.one_listview,  
                    new String[] {"deFavorName"},   
                    new int[] {R.id.OneItem});

	 		
	         favList.setAdapter(mSchedule1);
	         favList.setOnItemClickListener(new OnItemClickListener() {
					public void onItemClick(AdapterView<?> parent, View view, int position, long id) {  
					ListView lv = (ListView)parent;  
					@SuppressWarnings("unchecked")
					HashMap<String,Object> dbr =(HashMap<String,Object>)lv.getItemAtPosition(position);   
					String favName = (String) dbr.get("favorName");
					
					//Es wird per Intent von Activity DBpedia nach Activity ZweitSuche weitergeleitet.
					//String entity_data wird als "entity_data" an Activity ErstSuche übergegeben. String favor wird als "ItemEntity" an Activity ErstSuche übergegeben.
					Intent intent = new Intent();
	                intent.setClass(DBpedia.this, ReadFavorit.class);
	                intent.putExtra("favName",favName);
	                startActivity(intent);
					}
		     }); 
		     	
	         favList.setOnItemLongClickListener(new OnItemLongClickListener(){
					public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
						String fName = (((TextView) ((LinearLayout) view).getChildAt(0)).getText().toString());
						String path = Environment.getExternalStorageDirectory().toString();
						final File file = new File(path + File.separator +"DBpedia" + File.separator + fName);
						
						AlertDialog.Builder builder = new Builder(DBpedia.this);
						builder.setMessage("Are you sure you want to delete this file?");
						builder.setTitle("Delete file");
						builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								if (file.exists()) {  
						            file.delete();
						            getFavorites();
						        }  
								else {
									Toast.makeText(DBpedia.this, "deleted file false", Toast.LENGTH_LONG).show();
								}
							}
						});
						builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
						   public void onClick(DialogInterface dialog, int which) {
							   }
						});
						AlertDialog alertDialog = builder.create();
						alertDialog.show();
						return false;
					}
			      }); 
		}
	}

	//Suchverlauf als ListView auf dem Bildschirm zu zeigen
	private void getHistory(String str) {
		textview1.setTextSize(14);
		textview1.setGravity(Gravity.CENTER);
		textview1.setText("Last 50 Search History:");
		
		sp = getSharedPreferences("Suchverlauf", Context.MODE_PRIVATE);  
		String longhistory = sp.getString(str, ",").substring(1); 
		
		String[] histories = longhistory.split(",");  
		
		ArrayAdapter<String> adapter_history = new ArrayAdapter<String>(this,  
    		  R.layout.one_listview, R.id.OneItem, histories);  
		if(null != favList){
			favList.setVisibility(View.GONE);
			hisList.setVisibility(View.VISIBLE);
        }
		hisList.setAdapter(adapter_history); 
		hisList.setOnItemClickListener(new OnItemClickListener() {
    	  public void onItemClick(AdapterView<?> parent, View view, int position, long id) {  
			String history = (((TextView) ((LinearLayout) view).getChildAt(0)).getText().toString());
			
			//Es wird per Intent von Activity DBpedia nach Activity ErstSuche weitergeleitet.
			//String entity_data wird als "entity_data" an Activity ErstSuche übergegeben. 
			Intent intent = new Intent();
			intent.setClass(DBpedia.this, SemanticSearch.class);
			intent.putExtra("text",history);
			startActivity(intent);
    	  	}
	    });
		
	 }
	private void delHistoryAll(){
		final AlertDialog.Builder builder = new Builder(DBpedia.this);
		builder.setMessage("Are you sure you want to clear all your search history?");
		builder.setTitle("Delete search history");
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				SharedPreferences.Editor editor = sp.edit();
				editor.clear(); 
				editor.commit();
			}
		});
		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		   public void onClick(DialogInterface dialog, int which) {
			   }
		});
		AlertDialog alertDialog = builder.create();
		alertDialog.show();
	}
	
	private void delFavoritAll(){
		final AlertDialog.Builder builder = new Builder(DBpedia.this);
		builder.setMessage("Are you sure you want to clear all your favorites?");
		builder.setTitle("Delete Favorites");
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				String path = Environment.getExternalStorageDirectory().toString();
				final File file = new File(path + File.separator +"DBpedia");
				
			        String[] favFile = file.list();
			        for (int i = 0; i < favFile.length; i++) {
			            new File(file, favFile[i]).delete();
			    }
			}
		});
		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		   public void onClick(DialogInterface dialog, int which) {
			   }
		});
		AlertDialog alertDialog = builder.create();
		alertDialog.show();
	}
	
}