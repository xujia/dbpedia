package app.dbpedia;

import java.io.File;
import java.io.FileOutputStream; 
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import android.os.Environment;
import android.widget.Toast;

public class SaveJson {
	File file;
	FileOutputStream fos;
	String query = null;
	OutputStream output = null;

public void save(String name, String textUri){
	query = "SELECT DISTINCT ?Property ?Value ?isPropertyOf ?isValue WHERE { {<"+textUri+">  ?Property ?Value.} union {?isValue ?isPropertyOf <"+textUri+">.}} order  by (<LONG::IRI_RANK>(?Property))";
	String URLQuery = null;
	try {
		URLQuery = java.net.URLEncoder.encode(query, "UTF-8");
	} catch (UnsupportedEncodingException e) {
		e.printStackTrace();
	}
	String saveQuery = "http://dbpedia.org/sparql?default-graph-uri=&query="+URLQuery+"&format=json&timeout=30000"; 
	try {
	URL url = new URL(saveQuery);
	URLConnection conn = url.openConnection();
	
	if(!Environment.getExternalStorageState().equals(    
            Environment.MEDIA_MOUNTED)){//Wenn SD-Karte nicht existiert, return
        return; 
    } 
	String filename = name;
	String path = Environment.getExternalStorageDirectory().toString();
	File file = new File(path + File.separator +"DBpedia"+ File.separator + filename);
	  
    InputStream input = conn.getInputStream();  
    if(!file.getParentFile().exists()){
        file.getParentFile().mkdirs();//wenn file.getParentFile nicht existiert, wird DBpedia Ordner erstellt
    }  
  
    output = new FileOutputStream(file);  
    byte[] buffer=new byte[4*1024]; 
    int length;
    while((length=input.read(buffer)) > 0){  
        output.write(buffer,0, length);  
    }  
    output.flush();  
    }  
    catch (MalformedURLException e) {  
    e.printStackTrace();
    } 
    catch (IOException e) {  
    e.printStackTrace();  
    }
    finally{  
            try {
				output.close();
			} 
            catch (IOException e) {
				e.printStackTrace();
			}  
			}
	}
}

