package app.dbpedia;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class SemanticResource extends Activity {
	private ListView searchlist3;
	private TextView textoben3;
	private View footView3;
	private Button back_button;
	private Button menu_button;
	private String data3 = null;
	private String text3 = null;
	private String text3Uri = null;
	private String countQuery = null;
	private int sum = 0;
	private SparqlAnfrage sparql = new SparqlAnfrage();
	private SaveJson sa = new SaveJson();
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.layout_semantic_resource);//Set the activity content from resource layout_dbpedia.xml
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.title_window_search);
        initView3();
        
    }
 
    private void initView3(){
        searchlist3 = (ListView) findViewById(R.id.SearchListView3);
        textoben3 = (TextView) findViewById(R.id.TextOben3);
        footView3 = getLayoutInflater().inflate(R.layout.foot_listview, null);
        text3Uri = this.getIntent().getStringExtra("ItemEntity");
        data3 = this.getIntent().getStringExtra("entity_data");
        text3 = text3Uri.substring(28);
        
        if (data3==null&text3!=null){zweitSuche();}
        else if (data3!=null&text3!=null) {entityData(data3, 0);}
        
        back_button = (Button) findViewById(R.id.back2);
        back_button.setOnClickListener(new OnClickListener(){
            public void onClick(View v){
            	SemanticResource.this.finish();}
            });
        
        menu_button = (Button) findViewById(R.id.setting2);
        menu_button.setOnClickListener(new OnClickListener(){
            public void onClick(View v){
            	openOptionsMenu();}
            });
    }
    
    //DBpedia Ressource suchen
    public void zweitSuche() {
		data3 = sparql.searchDBData(text3Uri, 0);
		sum = sparql.countDBdata(text3Uri);
		entityData(data3, 0);
    }
      
    @SuppressWarnings("deprecation")
	public void entityData(String data, int offset){
    	textoben3.setText(URLDecoder.decode(text3));
    	
    	if(searchlist3.getFooterViewsCount()>0)
  		{searchlist3.removeFooterView(footView3);}
    	footView(offset);
    	
		ArrayList<HashMap<String, Object >> list3 = new ArrayList<HashMap<String, Object >>(); 
    	try {   
    		JSONObject dbjsonOb = new JSONObject(data).getJSONObject("results");  
    		JSONArray dbjsonArray = dbjsonOb.getJSONArray("bindings"); 
    		
	    		for(int i = 0; i < dbjsonArray.length() ; i++){   
	    			HashMap<String, Object > map3 = null ;
	        		map3 = new HashMap<String, Object>();
		        	JSONObject item3 = dbjsonArray.getJSONObject(i);
		    		@SuppressWarnings("unchecked")
					Iterator<String> keys = item3.keys();  
		    	    String key;  
		    	    JSONObject object;  
		    	    while (keys.hasNext()) {  
		    	        key = keys.next();  
		    	        object = item3.getJSONObject(key);  
		    	        String keyValue = object.getString("value");
		    	        if (key.equals("Property")){
		    	        	int index1 = keyValue.lastIndexOf("/"); 
		    	        	String property = keyValue.substring(index1 + 1);
		    	        	map3.put("Property", property); 
		    	        	}
		    	        if (key.equals("Value")){ 
		    	        	try {
		    	        		keyValue = keyValue.replaceAll("%(?![0-9a-fA-F]{2})", "%25");
		    	        		keyValue = URLDecoder.decode(keyValue, "utf-8");
		    	             } catch (Exception e) {
		    	                e.printStackTrace();
		    	             }
		    	        	map3.put("Value", keyValue);  
		    	        	}
		    	        if (key.equals("isPropertyOf")){
		    	        	int index1 = keyValue.lastIndexOf("/"); 
		    	        	String isPropertyOf = keyValue.substring(index1 + 1);
		    	        	map3.put("Property", "is :"+isPropertyOf+" of");  
		    	        }
		    	        if (key.equals("isValue")){
		    	        	try {
		    	        		keyValue = keyValue.replaceAll("%(?![0-9a-fA-F]{2})", "%25");
		    	        		keyValue = URLDecoder.decode(keyValue, "utf-8");
		    	             } catch (Exception e) {
		    	                e.printStackTrace();
		    	             }
		    	        	map3.put("Value", keyValue);
		    	        }
		    	    }
		    	    list3.add(map3);
	    		}  
           
        } catch (JSONException e) {   
            System.out.println("Jsons parse error !");   
            e.printStackTrace();   
        }   
    	
    	final SimpleAdapter mSchedule3 = new SimpleAdapter (SemanticResource.this, list3,   
                R.layout.option_count_listview,  
                new String[] {"Property", "Value"},   
                new int[] {R.id.twoItem1,R.id.twoItem2});  
    	
		searchlist3.setAdapter(mSchedule3); 
	}
  
//Men�-Symbols f�r "History", "Favorites", "Add", "About"
  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
      getMenuInflater().inflate(R.menu.menu2, menu);
      return true;
  }
  
  public boolean onOptionsItemSelected(MenuItem item) {
  	        switch (item.getItemId()) {
  	       
	        case R.id.item01:
	        		addFavorites();
	            break;
  	        }
				return false;      
}

	private void addFavorites() {
		if (text3!=null&&data3!=null){
		sa.save(text3, text3Uri);
		Toast.makeText(this, "Success! Add "+text3+" to Favorites", Toast.LENGTH_LONG).show();}
	}

	private void footView(int n){
		final int num = n;
    	int a = n+1;
    	int b = n+20;
    	if (sum==0) a = 0;
    	if (sum<=b) b= sum;
    	
		 if(searchlist3.getFooterViewsCount()>0)
	  		{searchlist3.removeFooterView(footView3);}
	      	
  		searchlist3.addFooterView(footView3);
  		TextView text_unten = (TextView) findViewById(R.id.TextUnten);
  		text_unten.setText("show "+a+" - "+b+" of total "+ sum);
  		
  		Button btn_next = (Button) findViewById(R.id.btn_Next);
  		if (sum>b){
  			btn_next.setOnClickListener(new OnClickListener(){
  			public void onClick(View v) {
  				data3 = sparql.searchDBData(text3Uri, num+20);
  				entityData(data3, num+20);
  				
  			}
  			});
  		}
	  		
  		Button btn_back = (Button) findViewById(R.id.btn_Previous);
  		if (num>=20){
  			btn_back.setOnClickListener(new OnClickListener(){
  			public void onClick(View v) {
  				data3 = sparql.searchDBData(text3Uri, num-20);
  				entityData(data3, num-20);
  			}
  		});
  		}
	}
}