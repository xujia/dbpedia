package app.dbpedia;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.util.EncodingUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.util.JsonReader;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;












//import org.codehaus.jackson.map.*;
import org.codehaus.jackson.*;

public class ReadFavorit extends Activity {
	//private View footView4;
	private TextView textoben4;
	private String favName = null;
	private ListView searchlist4;
	private Button back_button;
	private Button menu_button;
	
	 @Override
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.layout_semantic_resource);//Set the activity content from resource layout_dbpedia.xml
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.title_window_search);
        initView4();
	        
	    }
	 @SuppressWarnings("deprecation")
	private void initView4() {
		 favName = this.getIntent().getStringExtra("favName");
		 searchlist4 = (ListView) findViewById(R.id.SearchListView3);
		 textoben4 = (TextView) findViewById(R.id.TextOben3);
		 textoben4.setText(URLDecoder.decode(favName));

		 back_button = (Button) findViewById(R.id.back2);
         back_button.setOnClickListener(new OnClickListener(){
            public void onClick(View v){
            	ReadFavorit.this.finish();}
            });
        
         menu_button = (Button) findViewById(R.id.setting2);
         menu_button.setOnClickListener(new OnClickListener(){
            public void onClick(View v){
            	openOptionsMenu();}
            });
		 readJsonFile(favName);
	}
	 
	 //Bibeliothek Jackson zu nutzen 
	 //die gespeicherte Json-Dateien zu parsen
	 @SuppressWarnings("deprecation")
	public void readJsonFile(String filename){
		JsonFactory jfactory = new JsonFactory();
		ArrayList<HashMap<String, Object >> list4 = new ArrayList<HashMap<String, Object >>();
		String path = Environment.getExternalStorageDirectory()+File.separator +"DBpedia"+ File.separator + filename;  
		try {
			JsonParser jParser = jfactory.createJsonParser(new File(path));
			String currentToken = null;
			String valueText = null;
			JsonToken curtoken = null;	
			while (jParser.nextToken() != JsonToken.END_OBJECT) {
				curtoken = jParser.getCurrentToken();
			    currentToken = jParser.getCurrentName();
				if ("bindings".equals(currentToken)) {
					jParser.nextToken();
					currentToken = jParser.getCurrentName();
					HashMap<String, Object > map4 = new HashMap<String, Object>();
					while(jParser.nextToken() != JsonToken.END_OBJECT){
							jParser.nextToken();
							currentToken = jParser.getCurrentName();
							
						    if ("Property".equals(currentToken)) {
						    	while(jParser.nextToken() != JsonToken.END_OBJECT){
							    	jParser.nextToken();
							    	currentToken = jParser.getCurrentName();
							    	if ("value".equals(currentToken)) {
							    	  jParser.nextToken();
							    	  valueText = jParser.getText();
							    	  int index1 = valueText.lastIndexOf("/"); 
							    	  String property = valueText.substring(index1 + 1);
							    	  map4.put("Property", property); 
							    	}
							    }
						    }
						    if ("Value".equals(currentToken)) {
						    	while(jParser.nextToken() != JsonToken.END_OBJECT){
							    	jParser.nextToken();	
							    	currentToken = jParser.getCurrentName();
						    		if ("value".equals(currentToken)) {
						    			valueText = jParser.getText();
						    			jParser.nextToken();
					    			try {
					    				valueText = valueText.replaceAll("%(?![0-9a-fA-F]{2})", "%25");
					    				valueText = URLDecoder.decode(valueText, "utf-8");
				    	             } catch (Exception e) {
				    	                e.printStackTrace();
				    	             }
					    			map4.put("Value", valueText);
						    	    list4.add(map4);
						    	    map4 = new HashMap<String, Object>();
						    	    curtoken = jParser.getCurrentToken();
							    	}
							    }
						    }
						    if ("isPropertyOf".equals(currentToken)) {
						    	while(jParser.nextToken() != JsonToken.END_OBJECT){
							    	jParser.nextToken();
							    	currentToken = jParser.getCurrentName();
							    	if ("value".equals(currentToken)) {
							    	  jParser.nextToken();
							    	  valueText = jParser.getText();
							    	  int index1 = valueText.lastIndexOf("/"); 
							    	  String isPropertyOf = valueText.substring(index1 + 1);
							    	  map4.put("Property", "is :"+URLDecoder.decode(isPropertyOf)+" of");
							    	}
							    }
						    }
						    if ("isValue".equals(currentToken)) {
						    	while(jParser.nextToken() != JsonToken.END_OBJECT){
							    	jParser.nextToken();
							    	currentToken = jParser.getCurrentName();
							    	if ("value".equals(currentToken)) {
							    	  
							    	  valueText = jParser.getText();
							    	  jParser.nextToken();
							    	  try {
						    				valueText = valueText.replaceAll("%(?![0-9a-fA-F]{2})", "%25");
						    				valueText = URLDecoder.decode(valueText, "utf-8");
					    	             } catch (Exception e) {
					    	                e.printStackTrace();
					    	             }
							    	  map4.put("Value", valueText); 
							    	  list4.add(map4);
							    	  map4 = new HashMap<String, Object>();
							    	  curtoken = jParser.getCurrentToken();
							    	}
							    }
						    }
						}
					}
				
				if (curtoken == JsonToken.END_OBJECT)
				break;
				
				else{
			    	curtoken = jParser.getCurrentToken();
			    jParser.nextToken();
			    }
				}
			jParser.close();
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
				}
		
		final SimpleAdapter mSchedule4 = new SimpleAdapter (ReadFavorit.this, list4,   
                R.layout.option_count_listview,  
                new String[] {"Property", "Value"},   
                new int[] {R.id.twoItem1,R.id.twoItem2}); 		
		searchlist4.setAdapter(mSchedule4); 
	 }
	 
}
		  
		
